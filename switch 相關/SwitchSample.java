public class SwitchSample{
	public static void main(String[] args) {
		int grade = 4;
		
		switch(grade){
			case 1:
				System.out.println("你是一年級");
				break;
			case 2:
				System.out.println("你是二年級");
				break;
			case 3:
				System.out.println("你是三年級");
				break;
			case 4:
				System.out.println("你是四年級");
				break;
		}
		
	}
}