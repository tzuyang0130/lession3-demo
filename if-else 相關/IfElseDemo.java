public class IfElseDemo{
	/**
	 * 判斷是否已經成年
	 * @param age 年齡
	 */
	public void checkAge(int age){
		if(age >= 18){
			System.out.println("此人已經成年");
		}else{
			System.out.println("此人尚未成年");
		}
	}
	
	public static void main(String[] args) {
		IfElseDemo demo = new IfElseDemo();
		demo.isAdult(10);
		demo.isAdult(18);
		demo.isAdult(20);
	}
}